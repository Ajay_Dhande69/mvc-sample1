class OrdersController < ApplicationController
  include ApplicationHelper
  @@msg = ""
  def checkout
    @order = current_order
    @shipping_count = @order.order_products.where({digital: false}).count
    redirect_to cart_path if @order.new_record?
  end

  def paypal_response
    app_payment = PaypalPayment.new
    payment_response = app_payment.execute_payment(params[:PayerID], params[:paymentId])
    if payment_response.state == "approved"
      @order = current_order
      @user = current_user
      phone_no = Address.find_by(id: @order.shipping_address_id).mobile
      # Address.where({:id => @order.shipping_address_id}).pluck(:mobile)
      payment = Payment.create(
        tender_id: params[:token],
        transaction_id: params[:paymentId],
        location_id:params[:PayerID],
        amount: current_order.permalink.nil? ? @order.price.to_f.round(2) : (params[:amount]+"."+params[:format]).to_f.round(2),
        order: @order,
        user: @user)
      if @user.nil? && @order.name.eql?("EmailDownloadBook")

        msg_resp = send_sell_msg(phone_no, @order)
        if msg_resp != true && !phone_no.blank?
          flash[:alert] = "Wrong mobile no. can't send OTP on the #{phone_no}, Please check mobile no."
          redirect_to checkout_path(current_order) and return
        end
        @order.update_attributes(complete: true, user_id: nil)
        send_mail_sms
        
        if current_user.nil?
          redirect_to complete_order_path(@order) and return
        else
          flash[:alert] = "Your order has been completed successfully."
          redirect_to projects_path and return
        end
        
      elsif @user.nil?
        # @@msg = send_otp(phone_no, @order)
        # if @@msg[0] != true && !phone_no.blank?
        #   flash[:alert] = "Wrong mobile no. can't send OTP on the #{phone_no}, Please check mobile no."
        #   redirect_to checkout_path(current_order) and return
        # end
        @order.update_attributes(complete: true, user_id: nil)
        send_mail_sms
        if current_user.nil?
          redirect_to complete_order_path(@order) and return
        else
          flash[:alert] = "Your order has been completed successfully."
          redirect_to projects_path and return
        end
      else
        @user.update_attributes(is_permanent: true, balance_amount: (current_user.balance_amount.nil? ? 0 : current_user.balance_amount) + (current_order.permalink.nil? ? @order.price.to_i : current_order.order_products.sum(:price).to_i))
        @order.update_attributes(complete: true, email: @user.email, user_id: @user.id)
        send_mail_sms
        if current_user.nil?
          redirect_to complete_order_path(@order) and return
        else
          flash[:alert] = "Your order has been completed successfully."
          redirect_to projects_path and return
        end
      end
        if current_user.nil?
          redirect_to complete_order_path(@order) and return
        else
          flash[:alert] = "Your order has been completed successfully."
          redirect_to projects_path and return
        end
    else
      flash[:alert] = payment_response
      redirect_to checkout_path(current_order) and return
    end
  end

  def process_card
    @order = current_order
    # params[:amount] = @order.order_products.sum(:price).to_f + Order::TAX_RATE + shipping_charges(@order.shipping_address.nil? ? 0 : @order.shipping_address.country, @order.order_products)
    @user = current_user
    phone_no = Address.where({:id => @order.shipping_address_id}).pluck(:mobile)
    @@msg = send_otp(phone_no, @order)
    if params[:ssl_result_message] == "APPROVAL"
      Payment.create(
        tender_id: params[:ssl_approval_code],
        transaction_id: params[:ssl_txn_id],
        amount: params[:ssl_amount].to_f.round(2))
      if @user.nil?
        @order.update_attributes(complete: true, user_id: 0)
        send_admin_sms(@order, @order.email)
        send_mail_sms
        redirect_to complete_order_path(@order.id) and return
      else
        @user.update_attributes(is_permanent: true, balance_amount: (current_user.balance_amount.nil? ? 0 : current_user.balance_amount) + params[:ssl_amount].to_i)
        @order.update_attributes(complete: true, email: @user.email, user_id: @user.id)
        send_mail_sms
        flash[:alert] = "Your order has been completed successfully."
        redirect_to projects_path and return
      end
    end
  
  end

  def send_mail_sms
    send_admin_sms(@order, @order.email)
    if @order.name.eql?("EmailDownloadBook")
      SaleMailer.sell_download_book(@order, request.base_url).deliver_later
    else
      SaleMailer.receipt(@order, request.base_url).deliver_later
    end
    if !AdminMobEmail.all.ids.blank?
      AdminMobEmail.all.pluck(:admin_email).each do |email|
        SaleMailer.receipt_by_admin(@order, @order.email, email).deliver_later
      end        
    else
      SaleMailer.receipt_by_admin(@order, @order.email, "taddaquay@gmail.com").deliver_later
    end    
  end

  def process_paypal
    @order = current_order
    @user = current_user
    @order_paramlink = @order.permalink
    params[:amount] = !@order_paramlink.nil? ? (@order.order_products.sum(:price).to_f + Order::TAX_RATE + shipping_charges(@order.shipping_address.nil? ? 0 : @order.shipping_address.country, @order.order_products)) : @order.price
    paypal_payment = PaypalPayment.new(order: @order, params: params)
    if params[:with_paypal] == "true"
      payment = @order_paramlink.nil? ? paypal_payment.order_pay_with_paypal(@order, "#{request.base_url+checkout_path(@order)}", "#{request.base_url+paypal_response_path}", "This is Book purchase order.") : paypal_payment.orders_pay_with_paypal(@order, params[:amount], "#{request.base_url+checkout_path(@order)}", "#{request.base_url+ paypal_orders_response_path(params[:amount])}", "This is Book purchase order.")
      if payment.create
        @redirect_url = payment.links.find{|v| v.rel == "approval_url" }.href
        redirect_to @redirect_url
      else
        flash[:alert] = payment.error.inspect
        redirect_to checkout_path(@order) and return
      end
    end
  end

  def resend_otp
    @subscription = Subscription.where({sub_mobile_no: params[:mobile_no]}).last
    if !@subscription.blank?
      sub_msg = send_sub_otp(@subscription.sub_mobile_no)
      if sub_msg == true
        @subscription.update_attributes(subscription_otp: @pin)
        render json: {messages: "OTP has been sent on your mobile no."}
      else
        render json: {messages: sub_msg}
      end
    else
      @address = Address.where({mobile: params[:mobile_no]}).last
      @order_email = @address.nil? ? "" : Order.find_by(shipping_address_id: @address.id).email
      if User.find_by(email: @order_email).blank? && !@address.blank?
        @order = Order.find_by(shipping_address_id: @address.id)
        @@msg = send_otp(params[:mobile_no], @order)
        render json: {messages: "Otp has been sent on your mobile no."}
      else
        render json: { messages: "Mobile number not registered,  Please do purchase subscription plan or Book to get OTP"}
        # messages: "Please create order or User mobile no. already used for signed up."
      end
    end
  end

  def destroy
    @order = Order.find_by(id: params[:id])
    @order.destroy
    flash[:notice] = "Order deleted Successfully."
    redirect_to cart_path
  end


  def send_otp(phone_no, order)
    @pin = rand(0000..9999).to_s.rjust(4, "0")
    begin
      client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
      response = client.messages.create(
      from: ENV['TWILIO_NUMBER'],
      to:   phone_no,
      body: "\nYour OTP for www.greenxprize.com is \n #{@pin} and Registration Email is #{order.email}")
      order.update_attributes(order_otp: @pin)
      return [true, @pin]
    rescue Twilio::REST::RequestError => e
      return e.message
    end
  end

  def complete
    @order = Order.find_by_id(params[:id])
    @permalink = @order.permalink if !@order.permalink.nil?
    # in order to view orders via email for downlaods
    # if params[:token]
    #   if @order && @order.token == params[:token]
    #     session[:order_id] = @order.id
    #   end
    # end
    if session[:order_id] && session[:order_id] == @order.id
      session[:order_id] = nil
      # if ["EmailDownloadBook", "Lifetime"].include?(@order.name)
        flash[:success] = "Your order is complete."
      # elsif current_user.nil? && !@order.order_otp.blank?
      #   flash[:success] = "Your order is complete. OTP has been sent on your mobile no."
      # elsif @order.order_otp.blank? && current_user.nil?
      #   flash[:warning] = "#{@@msg}"
      #   redirect_to root_path
      # else
      #   flash[:success] = "Your order is complete."
      # end
    else
      flash[:warning] = "Your need a valid authentication token to view your order. Check your email for the correct link."
      redirect_to root_path
    end
  end

  # original method added by metova
  # def mail_sending
  #   @order = current_order || Order.last
  #   session[:order_id] = nil
  #   flash[:success] = "Your order is complete. Email on the way!"
  #   redirect_to root_path
  # end

  private
  def send_sub_otp(phone_no)
    begin
      @pin = rand(0000..9999).to_s.rjust(4, "0")
      client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
      response = client.messages.create(
      from: ENV['TWILIO_NUMBER'],
      to:   phone_no,
      body: "\nyour otp for www.greenxprize.com is \n #{@pin}")
      return true
    rescue Twilio::REST::RequestError => e
      return e.message
    end
  end

  def send_sell_msg(phone_no, order)
    if !phone_no.blank?
      begin
        client = Twilio::REST::Client.new ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN']
        response = client.messages.create(
        from: ENV['TWILIO_NUMBER'],
        to:   phone_no,
        body: "Hi #{Address.find_by(id: order.shipping_address_id).name.capitalize},\nThe Download verson is sent on your provided email address.")
        return true
      rescue Twilio::REST::RequestError => e
        return e.message
      end
    else
      return true
    end
  end
  
end